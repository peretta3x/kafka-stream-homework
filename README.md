# MESSAGING - Kafka Stream

### Prerequisites
- Download and install Java 8+ (Java 8 is already deprecated by newer Kafka’s version, however, that’s still supported)
- Download and unpack Kafka on the local PC (to create needed topics, push initial messages to them and launch local Kafka broker if it’s needed)
- Have either a local or remote a Kafka broker (or brokers) somewhere to be able to connect to that for the tasks
- Have 6 Kafka topics: >task1-1 task1-2 task2 task3-1 task3-2 task4
- An empty (simple) Spring Boot project with Maven or another similar tool you are comfortable with
- Add the following dependencies for Kafka: >org.springframework.kafka:spring-kafka org.apache.kafka:kafka-streams org.apache.kafka:kafka-clients org.apache.kafka:kafka-streams-test-utils org.apache.kafka:kafka_2.13

Docker compose file was provided to turn the validation of tasks easier
### Practical tasks execution guide
First you need to access the container that Kafka is running
```bash
docker exec -it kafka-homework_kafka-1_1 bash
```
Then you can start to push messages to Kafka topics using this command:
```bash
kafka-console-producer -bootstrap-server localhost:9092 -topic task1
```
The rest of the guide of how to execute/validate the tasks can be found in the kafka_streams_homework.pdf available in the learn platform