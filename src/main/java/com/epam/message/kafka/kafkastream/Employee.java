package com.epam.message.kafka.kafkastream;

public record Employee(
    String name,
    String company,
    String position,
    Integer experience
) {}
