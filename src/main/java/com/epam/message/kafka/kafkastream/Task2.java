package com.epam.message.kafka.kafkastream;

import java.util.Arrays;
import java.util.Objects;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Task2 {

    private final Logger log = LoggerFactory.getLogger(Task2.class);
    private static final Serde<String> STRING_SERDE = Serdes.String();

    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder) {
        KStream<String, String> messageStream = streamsBuilder
            .stream("task2", Consumed.with(STRING_SERDE, STRING_SERDE));

        KStream<Integer, String> wordsStream = messageStream
            .filter((key, value) -> Objects.nonNull(value))
            .peek((key, value) -> log.info("Key: {}, Value: {}", key, value))
            .mapValues((ValueMapper<String, String>) String::toLowerCase)
            .flatMapValues(value -> Arrays.asList(value.split("\\W+")))
            .selectKey((k, v) -> v.length());

        KStream<Integer, String> shortWords = wordsStream
            .filter((key, value) -> (key < 10))
            .mapValues((key, value) -> {
                return "%s-short".formatted(value);
            });

        KStream<Integer, String> longWords = wordsStream
            .filter((key, value) -> (key >= 10))
            .mapValues((key, value) -> {
                return "%s-long".formatted(value);
            });

        KStream<Integer, String> shortAndLongWords = shortWords.merge(longWords)
            .peek((key, value) -> log.info("Transformed => Key: {}, Value: {}", key, value));


        shortAndLongWords
            .filter((key, value) -> value.contains("a"))
            .mapValues((key, value) -> value.substring(0, value.indexOf("-")))
            .peek((key, value) -> log.info("PASSED MESSAGE => Key: {}, Value: {}", key, value));
    }
}
