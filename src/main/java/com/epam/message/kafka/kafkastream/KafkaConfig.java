package com.epam.message.kafka.kafkastream;

import static org.apache.kafka.streams.StreamsConfig.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;

@Configuration
public class KafkaConfig {

    private static final int NUMBER_OF_PARTITIONS = 1;
    private static final short NUMBER_OF_REPLICATIONS = 1;

    
    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    KafkaStreamsConfiguration kStreamsConfig() {

        Map<String, Object> props = new HashMap<>();
        props.put(APPLICATION_ID_CONFIG, "streams-app");
        props.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        return new KafkaStreamsConfiguration(props);
    }

    @Bean
    NewTopic task11Topic() {
        return new NewTopic("task1-1", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }

    @Bean
    NewTopic task12Topic() {
        return new NewTopic("task1-2", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }

    @Bean
    NewTopic task2Topic() {
        return new NewTopic("task2", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }

    @Bean
    NewTopic task31Topic() {
        return new NewTopic("task3-1", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }

    @Bean
    NewTopic task32Topic() {
        return new NewTopic("task3-2", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }

    @Bean
    NewTopic task4Topic() {
        return new NewTopic("task4", NUMBER_OF_PARTITIONS, NUMBER_OF_REPLICATIONS);
    }
}
