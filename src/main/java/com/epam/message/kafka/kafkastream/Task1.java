package com.epam.message.kafka.kafkastream;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Task1 {
    
    private final Logger log = LoggerFactory.getLogger(Task1.class);
    private static final Serde<String> STRING_SERDE = Serdes.String();
    
    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder) {
        KStream<String, String> messageStream = streamsBuilder
            .stream("task1-1", Consumed.with(STRING_SERDE, STRING_SERDE));

        messageStream
            .peek((key, value) -> log.info("Key: {}, Value: {}", key, value))
            .to("task1-2");
    }
}
