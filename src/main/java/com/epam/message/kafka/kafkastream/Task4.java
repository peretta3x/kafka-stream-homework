package com.epam.message.kafka.kafkastream;

import java.util.Objects;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Task4 {

    private final Logger log = LoggerFactory.getLogger(Task3.class);
    private static final Serde<String> STRING_SERDE = Serdes.String();

    @Autowired
    void buildPipeline(final StreamsBuilder streamsBuilder) {
        Serde<Employee> serdeFrom = Serdes.serdeFrom(new EmployeeSerializer(), new EmployeDeserializer());
        streamsBuilder
            .stream("task4", Consumed.with(STRING_SERDE, serdeFrom))
            .filter((key, value) -> Objects.nonNull(value))
            .peek((key, value) -> log.info("Key: {}, Value: {}", key, value));
    }
}
