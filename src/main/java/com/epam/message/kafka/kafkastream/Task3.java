package com.epam.message.kafka.kafkastream;

import java.time.Duration;
import java.util.Objects;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.StreamJoined;
import org.apache.kafka.streams.kstream.ValueJoiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Task3 {

    private final Logger log = LoggerFactory.getLogger(Task3.class);
    private static final Serde<String> STRING_SERDE = Serdes.String();
    private static final Serde<Long> LONG_SERDE = Serdes.Long();

    @Autowired
    void buildPipeline(final StreamsBuilder streamsBuilder) {
        var readFromTopic3_1 = streamsBuilder
            .stream("task3-1", Consumed.with(STRING_SERDE, STRING_SERDE));
            
        var messageStreamWithNewKey3_1 = readFromTopic3_1
            .filter((key, value) -> Objects.nonNull(value) && value.contains(":"))
            .selectKey((key, value) -> Long.valueOf(value.split(":")[0]))
            .peek((key, value) -> log.info("Key: {}, Value: {}", key, value));

        var readFromTopic3_2 = streamsBuilder
            .stream("task3-2", Consumed.with(STRING_SERDE, STRING_SERDE));
            
        var messageStreamWithNewKey3_2 = readFromTopic3_2
            .filter((key, value) -> Objects.nonNull(value) && value.contains(":"))
            .selectKey((key, value) -> Long.valueOf(value.split(":")[0]))
            .peek((key, value) -> log.info("Key: {}, Value: {}", key, value));


        ValueJoiner<String, String, String> joiner = (message1, message2) -> message1.concat(message2);

        messageStreamWithNewKey3_1.join(
            messageStreamWithNewKey3_2,
            joiner,
            JoinWindows.ofTimeDifferenceAndGrace(Duration.ofMinutes(1), Duration.ofSeconds(30)),
            StreamJoined.with(LONG_SERDE, STRING_SERDE, STRING_SERDE))
            .peek((key, value) -> log.info("Key: {}, Joined Value: {}", key, value));

    }
}
