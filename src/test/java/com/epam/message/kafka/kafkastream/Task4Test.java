package com.epam.message.kafka.kafkastream;

import java.util.Properties;

import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Task4Test {
    private Task4 task4;

    @BeforeEach
    void setUp() {
        task4 = new Task4();
    }

    @Test
    void givenInputMessages_whenProcessed_thenShouldDeserializeUsingCustomSerde() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        task4.buildPipeline(streamsBuilder);
        Topology topology = streamsBuilder.build();

        try (TopologyTestDriver topologyTestDriver = new TopologyTestDriver(topology, new Properties())) {

            TestInputTopic<String, Employee> inputTopic = topologyTestDriver
                .createInputTopic("task4", new StringSerializer(), new EmployeeSerializer());

            inputTopic.pipeInput("1", new Employee("Rafael", "EPAM", "Developer", 12));
            inputTopic.pipeInput("2", null);
            inputTopic.pipeInput("3", new Employee("John Doe", "Consultant", "Developer", 20));
        }
    }
}
