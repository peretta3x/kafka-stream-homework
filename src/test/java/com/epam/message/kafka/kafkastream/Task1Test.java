package com.epam.message.kafka.kafkastream;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Properties;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class Task1Test {
    private Task2 task1;

    @BeforeEach
    void setUp() {
        task1 = new Task2();
    }

    @Test
    void givenInputMessages_whenProcessed_thenSendToOutputTopic() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        task1.buildPipeline(streamsBuilder);
        Topology topology = streamsBuilder.build();

        try (TopologyTestDriver topologyTestDriver = new TopologyTestDriver(topology, new Properties())) {

            TestInputTopic<String, String> inputTopic = topologyTestDriver
                .createInputTopic("task1-1", new StringSerializer(), new StringSerializer());

            TestOutputTopic<String, String> outputTopic = topologyTestDriver
                .createOutputTopic("task1-2", new StringDeserializer(), new StringDeserializer());

            inputTopic.pipeInput("1", "First event");
            inputTopic.pipeInput("2", "Second event");

            assertThat(outputTopic.readKeyValuesToList())
                .containsExactly(
                    KeyValue.pair("1", "First event"),
                    KeyValue.pair("2", "Second event")
                );
        }
    }
}
