package com.epam.message.kafka.kafkastream;

import java.time.Duration;
import java.util.Properties;

import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class Task3Test {
    private Task3 task3;

    @BeforeEach
    void setUp() {
        task3 = new Task3();
    }

    @Test
    void givenInputMessages_whenProcessed_thenJoinTheTopicMessages() throws InterruptedException {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        task3.buildPipeline(streamsBuilder);
        Topology topology = streamsBuilder.build();

        try (TopologyTestDriver topologyTestDriver = new TopologyTestDriver(topology, new Properties())) {

            TestInputTopic<String, String> inputTopic = topologyTestDriver
                .createInputTopic("task3-1", new StringSerializer(), new StringSerializer());

            TestInputTopic<String, String> inputTopic2 = topologyTestDriver
                .createInputTopic("task3-2", new StringSerializer(), new StringSerializer());

            inputTopic2.pipeInput("1", "1:MESSAGE");
            inputTopic.pipeInput("2", "2:message again");
            inputTopic2.pipeInput("2", "2:MESSAGE AGAIN");
            
            Thread.sleep(Duration.ofSeconds(5).toMillis());

            inputTopic.pipeInput("1", "1:message");
        }
    }
}
