package com.epam.message.kafka.kafkastream;

import java.util.Properties;

import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class Task2Test {
    private Task2 task2;

    @BeforeEach
    void setUp() {
        task2 = new Task2();
    }

    @Test
    void givenInputMessages_whenProcessed_thenSplitSentenceIntoWords() {
        StreamsBuilder streamsBuilder = new StreamsBuilder();
        task2.buildPipeline(streamsBuilder);
        Topology topology = streamsBuilder.build();

        try (TopologyTestDriver topologyTestDriver = new TopologyTestDriver(topology, new Properties())) {

            TestInputTopic<String, String> inputTopic = topologyTestDriver
                .createInputTopic("task2", new StringSerializer(), new StringSerializer());

            inputTopic.pipeInput("1", "First event MyBigWOrdINtheMiddle in the Pindamonhangaba");
            inputTopic.pipeInput("2", "Second event in Itaquaquecetuba");
            inputTopic.pipeInput("3", "01234567890123 1234 a0 a1234567890123");
        }
    }
}
